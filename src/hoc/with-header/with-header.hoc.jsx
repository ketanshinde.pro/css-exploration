import React from 'react'
import Header from '../../components/header/header.component'

const WithHeader = WrappedComponent => {
    const returnComponent = ({ topic }) => {
        return (
            <div>
                <div>
                    <Header topic={topic} />
                </div>
                <WrappedComponent />
            </div>
        )
    }
    return returnComponent
}

export default WithHeader