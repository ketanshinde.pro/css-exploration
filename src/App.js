import './App.css';
import { Switch, Route, Redirect } from 'react-router-dom'
import Selectors from './pages/selectors/selectors.page'
import WithHeader from './hoc/with-header/with-header.hoc';

const SelectorsWithHeader = WithHeader(Selectors)

function App() {
  return (
    <div>
      <Switch>
        <Route exact path='/' component={() => { return <Redirect to='selectors' /> }} />
        <Route path='/selectors' render={() => <SelectorsWithHeader topic="Selectors" />} />
      </Switch>
    </div>
  );
}

export default App;
