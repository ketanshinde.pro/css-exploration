import React, { Component } from 'react'
import './header.styles.css'
import logo from '../../assets/images/logo.png'

export default class Header extends Component {
    render() {
        let { topic } = this.props

        return (
            <div className="headerRoot">
                <p className="headerText" >CSS</p>
                <div className="headerLogo">
                    <img className="headerLogoImg" src={logo} alt="Logo Here" />
                </div>
                <p className="headerText" >EXPLORATION</p>
                <div className="headerTopicDiv">
                    <p className="headerTopic">{topic}</p>
                </div>
            </div>
        )
    }
}
